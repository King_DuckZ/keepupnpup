/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gtest/gtest.h"
#include "rapidcheck/gtest.h"
#include "redirection.hpp"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>

namespace {
    std::string make_ip(uint8_t parA, uint8_t parB, uint8_t parC, uint8_t parD) {
        std::ostringstream oss;
        oss <<
            static_cast<int>(parA) << '.' <<
            static_cast<int>(parB) << '.' <<
            static_cast<int>(parC) << '.' <<
            static_cast<int>(parD)
        ;
        return oss.str();
    }

    rc::Gen<std::string> generate_lan_ip() {
        using namespace rc;

        auto ip_any = gen::arbitrary<uint8_t>();
        auto ip_pos = gen::positive<uint8_t>();

        std::vector<Gen<std::string>> gens;
        gens.push_back(gen::apply([](uint8_t c, uint8_t d) { return make_ip(192, 168, c, d); }, ip_any, ip_pos));
        gens.push_back(gen::apply([](uint8_t b, uint8_t c, uint8_t d) { return make_ip(10, b, c, d); }, ip_any, ip_any, ip_pos));
        gens.push_back(gen::apply([](uint8_t b, uint8_t c, uint8_t d) { return make_ip(172, b, c, d); }, gen::inRange<uint8_t>(16, 31), ip_any, ip_pos));

        return gen::join(gen::elementOf(gens));
    }

    rc::Gen<std::string> generate_external_ip() {
        using namespace rc;
        auto ip_a = gen::suchThat(gen::inRange<uint8_t>(1, 254), [](uint8_t v) { return 192 != v and 10 != v and 172 != v; });
        auto ip_b = gen::arbitrary<uint8_t>();
        auto ip_c = gen::arbitrary<uint8_t>();
        auto ip_d = gen::positive<uint8_t>();

        return gen::apply(&make_ip, ip_a, ip_b, ip_c, ip_d);
    }

    rc::Gen<std::string> generate_description() {
        using namespace rc;
        return gen::nonEmpty(gen::resize(80, gen::container<std::string>(gen::inRange<char>('a', 'z'))));
    }
} //unnamed namespace

namespace rc {
    template <>
    struct Arbitrary<kuu::Redirection> {
        static Gen<kuu::Redirection> arbitrary() {
            using kuu::Redirection;

            return gen::build<Redirection>(
                gen::set(&Redirection::internal_client, generate_lan_ip()),
                gen::set(&Redirection::remote_host, generate_external_ip()),
                gen::set(&Redirection::desc, generate_description()),
                gen::set(&Redirection::protocol, gen::element<kuu::Protocol>(kuu::Protocol::TCP, kuu::Protocol::UDP)),
                gen::set(&Redirection::duration, gen::just<uint32_t>(0)),
                gen::set(&Redirection::internal_port),
                gen::set(&Redirection::external_port),
                gen::set(&Redirection::enabled, gen::just<bool>(true))
            );
        }
    };
} //namespace rc

RC_GTEST_PROP(kuu, upnp, (const std::vector<kuu::Redirection>& parRedirections)) {
    //for (auto& red : parRedirections) {
    //    std::cout << "internal_client: " << red.internal_client << '\n';
    //    std::cout << "remote_host: " << red.remote_host << '\n';
    //    std::cout << "desc: \"" << red.desc << "\" (" << red.desc.size() << ")\n";
    //    std::cout << "protocol: " << red.protocol << '\n';
    //    std::cout << "duration: " << red.duration << '\n';
    //    std::cout << "internal_port: " << red.internal_port << '\n';
    //    std::cout << "external_port: " << red.external_port << '\n';
    //    std::cout << "enabled: " << red.enabled << '\n';
    //    std::cout << "---------------------------------------------------------\n";
    //}
}
