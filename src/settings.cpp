/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.hpp"
#include <yaml-cpp/yaml.h>
#include <ciso646>
#include <stdexcept>
#include <boost/algorithm/string.hpp>

namespace kuu {
	namespace {
		void load_bindings(RedirectSetting::PortMappingsList& parMappings, Protocol parProtocol, const YAML::Node& parNode) {
			if (parNode) {
				if (parNode.IsSequence()) {
					parMappings.reserve(parMappings.size() + parNode.size());
					for (std::size_t z = 0; z < parNode.size(); ++z) {
						const auto port = parNode[z].as<uint16_t>();
						parMappings.push_back(PortMapping(port, port, parProtocol));
					}
				}
				else if (parNode.IsScalar()) {
					parMappings.reserve(parMappings.size() + 1);
					const auto port = parNode.as<uint16_t>();
					parMappings.push_back(PortMapping(port, port, parProtocol));
				}
				else {
					throw std::runtime_error("Expected single port or port list");
				}
			}
		}

		bool to_boolean (const std::string& parValue) {
			using boost::iequals;

			return iequals(parValue, std::string("on")) or
				iequals(parValue, std::string("true")) or
				parValue == "1" or
				iequals(parValue, std::string("enabled"))
			;
		}
	} //unnamed namespace

	RedirectSetting load_redirect_settings (const std::string& parPath, const std::string& parLanAddr) {
		auto settings = YAML::LoadFile(parPath);
		auto redirect_node = settings["redirect"];
		if (not redirect_node)
			throw std::runtime_error("Can't find \"redirect\" key in yaml configuration");
		if (not redirect_node.IsMap())
			throw std::runtime_error("\"redirect\" key is not a map");

		RedirectSetting redir_settings;
		redir_settings.host = "self";
		if (redirect_node["host"]) {
			if (not redirect_node["host"].IsScalar())
				throw std::runtime_error("\"host\" node is not a scalar");
			redir_settings.host = redirect_node["host"].as<std::string>();
		}
		if (redir_settings.host == "self")
			redir_settings.host = parLanAddr;

		load_bindings(redir_settings.port_mappings, Protocol::TCP, redirect_node["tcp"]);
		load_bindings(redir_settings.port_mappings, Protocol::UDP, redirect_node["udp"]);

		redir_settings.desc = "KeepUPNPUp redirect";
		if (redirect_node["desc"]) {
			if (not redirect_node["desc"].IsScalar())
				throw std::runtime_error("\"desc\" node is not a scalar");
			redir_settings.desc = redirect_node["desc"].as<std::string>();
		}

		if (redirect_node["autoremove"])
			redir_settings.autoremove_old = to_boolean(redirect_node["autoremove"].as<std::string>());
		else
			redir_settings.autoremove_old = false;
		if (redirect_node["dropunused"])
			redir_settings.drop_unused = to_boolean(redirect_node["dropunused"].as<std::string>());
		else
			redir_settings.drop_unused = false;

		return redir_settings;
	}
} //namespace kuu
