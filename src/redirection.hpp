/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "enum.h"
#include <string>
#include <cstdint>

namespace kuu {
	BETTER_ENUM (Protocol, uint8_t,
		UDP,
		TCP,
		Other
	);

	struct Redirection {
		Redirection() : protocol(Protocol::Other) {}

		std::string internal_client;
		std::string remote_host;
		std::string desc;
		Protocol protocol;
		uint32_t duration;
		uint16_t internal_port;
		uint16_t external_port;
		bool enabled;
	};
} //namespace kuu
