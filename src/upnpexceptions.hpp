/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdexcept>
#include <string>

namespace kuu {
	class Exception : public std::runtime_error {
	protected:
		explicit Exception (const char* parMessage) : std::runtime_error(parMessage) {}
		explicit Exception (const std::string& parMessage) : std::runtime_error(parMessage) {}
	};

	class UPNPException : public Exception {
	public:
		UPNPException (int parError, const char* parMessageIntro, const char* parMessage);
		virtual ~UPNPException() noexcept = default;

		int error_code() const { return m_error; }

	private:
		int m_error;
	};

	class ScanException : public Exception {
	public:
		explicit ScanException (const char* parMessage);
		explicit ScanException (const std::string& parMessage);
	};
} //namespace kuu
