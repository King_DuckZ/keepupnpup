/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "redirection.hpp"
#include "upnpexceptions.hpp"
#include <memory>
#include <string>
#include <vector>
#include <cstdint>

namespace kuu {
	class UPNP {
	public:
		enum IGDReply {
			IGDValid = 1,
			IGDNotConnected = 2,
			IGDUpnpNotSureIGD = 3,
			IGDNotSure,
			IGDNone
		};

		UPNP();
		~UPNP() noexcept;

		IGDReply igd_reply() const;
		const std::string& lanaddr() const;
		const std::string& externaladdr() const;
		std::vector<Redirection> redirections() const;

		void add_port_mapping (
			uint16_t parExtPort,
			uint16_t parIntPort,
			const std::string& parAddr,
			const std::string& parDesc,
			uint32_t parDuration,
			Protocol parProtocol
		);

		void remove_port_mapping (
				uint16_t parExtPort,
				const std::string& parAddr,
				Protocol parProtocol
		);

	private:
		struct LocalData;

		std::unique_ptr<LocalData> m_local_data;
	};
} //namespace kuu
