/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#include "upnpexceptions.hpp"
#include <sstream>

namespace kuu {
	namespace {
		std::string build_exception_msg (int parError, const char* parMessageIntro, const char* parMessage) {
			std::ostringstream oss;
			oss << parMessageIntro << " (" << parError << ")";
			if (parMessage)
				oss << ": \"" << parMessage << '"';
			return oss.str();
		}
	} //unnamed namespace

	UPNPException::UPNPException (int parError, const char* parMessageIntro, const char* parMessage) :
		Exception(build_exception_msg(parError, parMessageIntro, parMessage)),
		m_error(parError)
	{
	}

	ScanException::ScanException (const std::string& parMessage) :
		Exception(parMessage)
	{
	}

	ScanException::ScanException (const char* parMessage) :
		Exception(parMessage)
	{
	}
} //namespace kuu
