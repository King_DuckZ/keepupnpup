/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "redirection.hpp"
#include <string>
#include <vector>
#include <cstdint>

namespace kuu {
	struct PortMapping {
		PortMapping (uint16_t parPort, uint16_t parInternalPort, Protocol parProtocol) :
			port(parPort),
			internal_port(parInternalPort),
			protocol(parProtocol)
		{
		}

		uint16_t port;
		uint16_t internal_port;
		Protocol protocol;
	};

	struct RedirectSetting {
		using PortMappingsList = std::vector<PortMapping>;
		std::string host;
		std::string desc;
		PortMappingsList port_mappings;
		bool autoremove_old;
		bool drop_unused;
	};

	RedirectSetting load_redirect_settings (const std::string& parPath, const std::string& parLanAddr);
} //namespace kuu
