/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#include "upnp.hpp"
#include "miniupnpc.h"
#include "upnpcommands.h"
#include "upnperrors.h"
#include "enum.h"
#include <array>
#include <algorithm>
#include <sstream>
#include <ciso646>
#include <strings.h>
#include <cassert>
#include <cstdint>

#if !defined(NDEBUG)
#	define KUU_VERBOSE
#endif

#if defined(KUU_VERBOSE)
#	include <iostream>
#endif

namespace kuu {
	BETTER_ENUM(UPNPDeleteErrors, uint16_t,
		InvalidArgs = 402, //See UPnP Device Architecture section on Control.
		NotAuthorized = 606, //The action requested REQUIRES authorization and the sender was not authorized.
		NoSuchEntryInArray = 714 //The specified value does not exist in the array
	);

	namespace {
		void freeUPNPDevlist (struct UPNPDev* parDele) {
#if defined(KUU_VERBOSE)
			std::cout << "Deleting UPNPDev " << parDele << '\n';
#endif
			::freeUPNPDevlist(parDele);
		}

		void FreeUPNPUrls (struct UPNPUrls* parDele) {
#if defined(KUU_VERBOSE)
			std::cout << "Deleting UPNPUrls " << parDele << '\n';
#endif
			::FreeUPNPUrls(parDele);
		}

		struct UPNPUrlsWithInitFlag {
			UPNPUrlsWithInitFlag() : initialized(false) {}
			explicit UPNPUrlsWithInitFlag(std::nullptr_t) : initialized(false) {}
			explicit UPNPUrlsWithInitFlag(bool parInit) : initialized(parInit) {}
			struct UPNPUrls urls;
			bool initialized;

			operator struct UPNPUrls* () { return (initialized ? &urls : nullptr); }
			UPNPUrls* operator-> () { return (initialized ? &urls : nullptr); }
			bool operator not() const { return not initialized; }
		};

		//see: http://stackoverflow.com/questions/24611215/one-liner-for-raii-on-non-pointer
		struct UPNPUrlDeleter {
			typedef UPNPUrlsWithInitFlag pointer;
			void operator() (UPNPUrlsWithInitFlag& parUrls) {
				if (parUrls.initialized) {
					parUrls.initialized = false;
					kuu::FreeUPNPUrls(&parUrls.urls);
				}
			}
		};

		using UPNPUrlsPtr = std::unique_ptr<UPNPUrlsWithInitFlag, UPNPUrlDeleter>;
		using UPNPDevsPtr = std::unique_ptr<struct UPNPDev, void(*)(struct UPNPDev*)>;

		std::vector<struct UPNPDev*> dev_list_to_vector (struct UPNPDev* parList) {
			std::vector<struct UPNPDev*> retval;
			for (auto dev = parList; dev; dev = dev->pNext) {
				retval.push_back(dev);
			}
			return retval;
		}

		bool is_enabled (const std::string& parEnabled) {
			if (parEnabled.size() == 4) {
				if (strncasecmp("true", parEnabled.c_str(), 4) == 0)
					return true;
			}
			else if (parEnabled.size() == 2) {
				if (strncasecmp("on", parEnabled.c_str(), 2) == 0)
					return true;
			}
			else if (parEnabled.size() == 3) {
				if (strncasecmp("yes", parEnabled.c_str(), 3) == 0)
					return true;
			}
			if (parEnabled == "1")
				return true;
			return false;
		}

#if defined(KUU_VERBOSE)
		void print_devices (
			const std::vector<struct UPNPDev*>& parDevs,
			struct UPNPUrls* parUrls,
			UPNP::IGDReply parIGDReply,
			const std::string& parLanAddr,
			const std::string& parExternalAddr
		) {
			std::cout << "Found: " << parDevs.front()->descURL << ", " << parDevs.front()->st << '\n';

			switch (parIGDReply) {
			case UPNP::IGDValid:
				std::cout << "Found valid IGD: " << parUrls->controlURL << '\n';
				break;
			case UPNP::IGDNotConnected:
				std::cout << "Found a (not connected?) IGD: " << parUrls->controlURL << '\n';
				break;
			case UPNP::IGDUpnpNotSureIGD:
				std::cout << "UPnP device found. Is it an IGD? " << parUrls->controlURL << '\n';
				break;
			case UPNP::IGDNotSure:
				std::cout << "Found device (igd?): " << parUrls->controlURL << '\n';
			}

			std::cout << "Local LAN ip address: " << parLanAddr << '\n';
			std::cout << "External ip address: " << parExternalAddr << '\n';
		}
#endif
	} //unnamed namespace

	struct UPNP::LocalData {

		LocalData() :
			lanaddr(),
			externaladdr(),
			igd_reply(IGDNone),
			devlist(nullptr, &kuu::freeUPNPDevlist)
		{
		}

		~LocalData() noexcept {
#if defined(KUU_VERBOSE)
			std::cout << "Destroying LocalData " << this << '\n';
#endif
		}

		std::string lanaddr;
		std::string externaladdr;
		struct IGDdatas data;
		IGDReply igd_reply;
		UPNPDevsPtr devlist;
		UPNPUrlsPtr urls;
	};

	UPNP::UPNP() :
		m_local_data(std::make_unique<LocalData>())
	{
		int error;
		UPNPDevsPtr devlist(
			upnpDiscover(
				2000,
				nullptr, //multicastif
				nullptr, //minissdpdpath
				UPNP_LOCAL_PORT_ANY, //localport
				0, //ipv6
				2, //ttl
				&error
			),
			&kuu::freeUPNPDevlist
		);
		if (error)
			throw UPNPException(error, "Error initializing upnpc in upnpDiscover()", nullptr);

		const auto devs = dev_list_to_vector(devlist.get());
		if (devs.empty())
			throw ScanException("No UPNP devices found");
		else if (devs.size() > 1)
			throw ScanException("More than one UPNP device found (" + std::to_string(devs.size()) + ")");

		UPNPUrlsPtr urls;
		assert(not urls.get());
		{
			UPNPUrlsWithInitFlag urls_local(true);
			assert(urls_local.initialized);
			std::array<char, 64> lanaddr;
			const auto i = UPNP_GetValidIGD(
				devlist.get(),
				urls_local,
				&m_local_data->data,
				lanaddr.data(),
				lanaddr.size()
			);
			urls.reset(urls_local);

			switch (i) {
			case 1:
			case 2:
			case 3:
				m_local_data->igd_reply = static_cast<IGDReply>(i);
				break;
			default:
				m_local_data->igd_reply = IGDNotSure;
			}

			m_local_data->lanaddr = lanaddr.data();
		}

		{
			std::array<char, 64> externaladdr;
			const auto r = UPNP_GetExternalIPAddress(urls->controlURL, m_local_data->data.first.servicetype, externaladdr.data());
			if (UPNPCOMMAND_SUCCESS != r)
				m_local_data->externaladdr = externaladdr.data();
		}

#if defined(KUU_VERBOSE)
		print_devices(devs, urls.get(), m_local_data->igd_reply, m_local_data->lanaddr, m_local_data->externaladdr);
#endif
		m_local_data->urls.swap(urls);
		m_local_data->devlist.swap(devlist);
	}

	UPNP::~UPNP() noexcept = default;

	UPNP::IGDReply UPNP::igd_reply() const {
		return m_local_data->igd_reply;
	}

	const std::string& UPNP::lanaddr() const {
		return m_local_data->lanaddr;
	}

	const std::string& UPNP::externaladdr() const {
		return m_local_data->externaladdr;
	}

	std::vector<Redirection> UPNP::redirections() const {
		char intClient[40];
		char intPort[6];
		char extPort[6];
		char protocol[4];
		char desc[80];
		char enabled[6];
		char rHost[64];
		char duration[16];
		int r;

		int z = 0;
		std::vector<Redirection> retval;
		do {
			rHost[0] = '\0';
			enabled[0] = '\0';
			duration[0] = '\0';
			desc[0] = '\0';
			extPort[0] = '\0';
			intPort[0] = '\0';
			intClient[0] = '\0';

			r = UPNP_GetGenericPortMappingEntry(
				m_local_data->urls->controlURL,
				m_local_data->data.first.servicetype,
				std::to_string(z).c_str(),
				extPort,
				intClient,
				intPort,
				protocol,
				desc,
				enabled,
				rHost,
				duration
			);

			if (not r) {
				Redirection redir;
				better_enums::optional<Protocol> proto = Protocol::_from_string_nothrow(protocol);
				redir.protocol = (proto ? *proto : +Protocol::Other);
				redir.internal_client = intClient;
				redir.remote_host = rHost;
				redir.desc = desc;
				redir.duration = static_cast<uint32_t>(stoul(std::string(duration)));
				redir.internal_port = static_cast<uint16_t>(stoul(std::string(intPort)));
				redir.external_port = static_cast<uint16_t>(stoul(std::string(extPort)));
				redir.enabled = is_enabled(std::string(enabled));

				retval.push_back(redir);
			}
			else if (713 != r) {
				throw UPNPException(r, "Error in invocation of UPNP_GetGenericPortMappingEntry()", strupnperror(r));
			}

			++z;
		} while (not r);
		return retval;
	}

	void UPNP::add_port_mapping (
		uint16_t parExtPort,
		uint16_t parIntPort,
		const std::string& parAddr,
		const std::string& parDesc,
		uint32_t parDuration,
		Protocol parProtocol
	) {
		const auto r = UPNP_AddPortMapping(
			m_local_data->urls->controlURL,
			m_local_data->data.first.servicetype,
			std::to_string(parExtPort).c_str(),
			std::to_string(parIntPort).c_str(),
			parAddr.c_str(),
			parDesc.c_str(),
			parProtocol._to_string(),
			0,
			std::to_string(parDuration).c_str()
		);
		if (UPNPCOMMAND_SUCCESS != r) {
			std::ostringstream oss;
			oss << "Error in UPNP_AddPortMapping(\"" <<
				m_local_data->urls->controlURL << "\", \"" <<
				m_local_data->data.first.servicetype << "\", " <<
				parExtPort << ", " <<
				parIntPort << ", \"" <<
				parAddr << "\", \"" <<
				parDesc << "\", " <<
				parProtocol._to_string() << ")";
			throw UPNPException(r, oss.str().c_str(), strupnperror(r));
		}
	}

	void throw_if_dele_failed (int parRetval, uint16_t parExtPort, const std::string& parAddr, Protocol parProtocol) {
		if (not parRetval)
			return;

		std::ostringstream oss;
		oss << "Unable to delete " << parProtocol << ' ' << parAddr <<
			':' << parExtPort << " - error " << parRetval << ": ";

		switch (parRetval) {
		case UPNPDeleteErrors::NotAuthorized:
			oss << "The action requested REQUIRES authorization and the sender was not authorized";
			break;

		case UPNPDeleteErrors::NoSuchEntryInArray:
			oss << "The specified value does not exist in the array";
			break;

		case UPNPDeleteErrors::InvalidArgs:
			oss << "See UPnP Device Architecture section on Control";
			break;

		default:
			oss << "Unknown error";
		}

		throw UPNPException(parRetval, oss.str().c_str(), strupnperror(parRetval));
	}

	void UPNP::remove_port_mapping (
		uint16_t parExtPort,
		const std::string& parAddr,
		Protocol parProtocol
	) {
		const int dele_retval = UPNP_DeletePortMapping(
			m_local_data->urls->controlURL,
			m_local_data->data.first.servicetype,
			std::to_string(parExtPort).c_str(),
			parProtocol._to_string(),
			parAddr.c_str()
		);

		throw_if_dele_failed(dele_retval, parExtPort, parAddr, parProtocol);
	}
} //namespace kuu
