/* Copyright 2016, Michele Santullo
 * This file is part of "keepupnpup".
 *
 * "keepupnpup" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "keepupnpup" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "keepupnpup".  If not, see <http://www.gnu.org/licenses/>.
 */

#include "upnp.hpp"
#include "settings.hpp"
#include <iostream>
#include <vector>
#include <string>
#include <ciso646>
#include <boost/range/adaptor/filtered.hpp>
#include <algorithm>

namespace {
	enum CurrentMappingType {
		NoMapping,
		PresentWithSameIP,
		PresentWithDifferentIP,
		IgnoreMapping
	};

	CurrentMappingType has_mapping (const std::vector<kuu::Redirection>& parRedirs, const std::string& parAddr, uint16_t parPort, kuu::Protocol parProtocol) {
		for (auto& redir : parRedirs) {
			if (redir.protocol == parProtocol and redir.internal_port == parPort)
				return (redir.internal_client == parAddr ? PresentWithSameIP : PresentWithDifferentIP);
		}
		return NoMapping;
	}

	void remove_unused_bindings(
		kuu::UPNP& parUpnp,
		const std::vector<kuu::Redirection>& parActiveRedirs,
		const std::string& parHost,
		const kuu::RedirectSetting::PortMappingsList& parPortMappings
	) {
		using boost::adaptors::filtered;

		auto redirs_to_host = parActiveRedirs |
			filtered([&](const kuu::Redirection& r) { return parHost == r.internal_client; });

		for (const auto& redir : redirs_to_host) {
			auto it_found = std::find_if(
				parPortMappings.begin(),
				parPortMappings.end(),
				[&](const kuu::PortMapping& m) {
					return redir.protocol == m.protocol and
						redir.external_port == m.port and
						redir.internal_port == m.internal_port;
				}
			);
			if (it_found == parPortMappings.end()) {
				parUpnp.remove_port_mapping(redir.external_port, parHost, redir.protocol);
			}
		}
	}

	void add_bindings (
		kuu::UPNP& parUpnp,
		const std::vector<kuu::Redirection>& parActiveRedirs,
		const std::string& parHost,
		const kuu::RedirectSetting::PortMappingsList& parPortMappings,
		const std::string& parDesc,
		bool parTryForcing
	) {
		for (auto& mapping : parPortMappings) {
			//std::cout << "Adding " << mapping.port << " --> " << parHost << ":" << mapping.internal_port << " " << mapping.protocol << '\n';
			bool try_add_mapping = true;
			try {
				const CurrentMappingType curr_mapping =
					has_mapping(parActiveRedirs, parHost, mapping.port, mapping.protocol);
				const CurrentMappingType decided_mapping =
					(PresentWithDifferentIP == curr_mapping and not parTryForcing ? IgnoreMapping : curr_mapping);

				switch (decided_mapping) {
				case PresentWithDifferentIP:
					parUpnp.remove_port_mapping(mapping.port, parHost, mapping.protocol);

				case NoMapping:
					parUpnp.add_port_mapping(mapping.port, mapping.internal_port, parHost, parDesc, 0, mapping.protocol);
					break;

				case PresentWithSameIP:
				case IgnoreMapping:
					break;
				};
			}
			catch (const kuu::UPNPException& e) {
				std::cerr << e.what() << std::endl;
			}
		}
	}

	void runapp() {
		kuu::UPNP upnp;

		kuu::RedirectSetting settings = kuu::load_redirect_settings("keepupnpup.yml", upnp.lanaddr());
		const std::vector<kuu::Redirection> redirs = upnp.redirections();

		add_bindings(upnp, redirs, settings.host, settings.port_mappings, settings.desc, settings.autoremove_old);
		if (settings.drop_unused)
			remove_unused_bindings(upnp, redirs, settings.host, settings.port_mappings);
	}
} //unnamed namespace

int main() {
	try {
		runapp();
	}
	catch (const kuu::Exception& e) {
		std::cerr << e.what() << std::endl;
		return 1;
	}

	return 0;
}

